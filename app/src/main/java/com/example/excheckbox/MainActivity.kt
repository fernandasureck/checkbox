package com.example.excheckbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkbox.setOnClickListener {
            if (checkbox.isChecked) {
                img.setColorFilter(resources.getColor(R.color.purple_200))
            } else {
                img.setColorFilter(resources.getColor(R.color.black))
            }
        }
    }
}